# OpenML dataset: Boston-Weather-Data-Jan-2013---Apr-2018

https://www.openml.org/d/43623

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Having just moved to Boston last fall, I wanted to know whether the severe winter weather I experienced was normal based on historical data, or if there are any patterns to when Boston gets heavy snowfall and wind.
Dataset contains highs, lows, and averages of temperature, dew point, wind speeds, sea level pressure, and precipitation levels for every day from 1/1/2013 - 4/8/2018 inclusive. All data is publicly available in its entirety on the Weather Underground website - this is a collation of data over several years, that is difficult to see all at once on the Weather Underground site. I simply cleaned up the data by breaking out the month and year into their own columns and condensing the column headers.
All data can be found here: https://www.wunderground.com/history/airport/KBOS/2018/1/1/DailyHistory.html?reqdb.zip=reqdb.magic=reqdb.wmo= 
It would be interesting to see this weather data compared with weather data from other locations to see if there are any corresponding patterns. Feel free to augment this dataset if you need more updated information or older information!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43623) of an [OpenML dataset](https://www.openml.org/d/43623). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43623/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43623/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43623/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

